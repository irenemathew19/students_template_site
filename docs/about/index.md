# About me

Hi! I am Irene Sara Mathew. I am a masters student at the University of Oulu in my first year. My major is Industrial Environmental Engineering. 

Visit this website to see my work!

## My background

I was born in a nice city called Kothamangalam which is in Kerala. Kerala is a small part of India but my family is settled in the UAE and I have been there since I was a baby.
I can speak English, Malayalam, Hindi and have also learnt French, Arabic and very little Finnish. 

## Why FabLab 

I have always been mesmerized by the way 3-D models have been built through FabLab. We used to have one in our college in India where I did my bachelors and 
I used to see my friends use it. I found it very cool and really wanted to know how it works. 

