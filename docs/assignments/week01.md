# 1. Principles and practices
This week I worked on defining my final project idea and started to getting used to the documentation process.

## How to open a new Repository and store changes from local to remote Repository

a) Make a file in your computer for FABLAB

b) Open Git Cmd and type 'ssh-keygen -t rsa -C "email" -b 4096'

![](../images/Capture.JPG)

c) Obtain private and public keys 
Upload the public key to the site given in Moodle. 

![](../images/Capture1.JPG)

![](../images/Capture2.JPG)

![](../images/Capture3.JPG)

![](../images/Capture4.JPG)

![](../images/Capture5.JPG)

![](../images/Capture6.JPG)

d) Go to gitlab.com/fabacademy_oulu/students_template_site and fork it so that 
it is available for you

![](../images/Capture7.JPG)

e) Type 'git init' in Git Cmd which will open a new repository

f) After you have to clone the repository from the site to your computer. This 
is done using 'git clone url' and that will be the local repository. 

![](../images/Capture8.JPG)

![](../images/Capture9.JPG)

![](../images/Capture10.JPG)

g) Make changes in the document via Notepad++ and save the changes. These are 
changes made in the local repository. 

![](../images/Capture11.JPG)

h) We have to store the changes made in the local repository
Done using 'git add --all'
Then we give user name and email using 'git config --global user.name "name" '
and 'git config --global user.email "email" '. 
Finally the changes are stored using git commit -m "message"

![](../images/Capture12.JPG)

![](../images/Capture13.JPG)

![](../images/Capture14.JPG)

i) Changes made are updated into the remote repository by using 'git push origin
master' and we can see the changes in GitLab. 

![](../images/Capture15.JPG)

![](../images/Capture16.JPG)


# 2. FINAL PROJECT 

The project I am planning to work on is ‘Utilization of Solar Generator without
the use of Inverter’.

The main objective of this project is to make an AC generator that uses solar energy 
and eliminate the use of converters, thereby reducing the losses. 

The main requirements of this project are: 

1.	Solar Panels
2.	Disc made of form board (non-conducting material) painted black 
3.	DC motor 
4.	Wooden base as support

Solar panels absorb the sunlight as it is made of Silicon and helps in the generation 
of electricity. Several solar panels (10) will be needed and a disc with half the 
number of slots as solar panels is to be made, the slots having same dimensions 
as the solar panel. The disc is needed to alternatively expose half of solar panels 
to sunlight. Solar panels will be connected in anti-parallel. 

DC motor is needed for rotating the disc over the solar panels that will be connected 
to the wooden base and the speed depends on the frequency of the motor. As the disc
rotates over the solar panel, it is expected that half of the alternate panels be 
exposed to sunlight and other half be covered from sunlight. By doing so, it is
expected to generate a sine wave when connected to a DSO. 

If not, then change the type of motor (vary frequency) so that the speed will be 
adjusted, and you get a sine wave. 

This can come of great advantage in countries with large amounts of solar energy
if it is made into a large system that can capture huge amounts of solar energy
and electricity can be produced from renewable sources of energy.

![](../images/image0.jpeg)

![](../images/2D_Representation_Project_File.JPG)



